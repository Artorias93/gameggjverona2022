using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO: Make pickup an interface
public interface Pickup
{
    void Use();
}
